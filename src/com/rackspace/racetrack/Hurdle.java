/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rackspace.racetrack;

/**
 *
 * @author user
 */
public class Hurdle extends TrackElement{
    private Double growthFactor;
    
    public Hurdle(Double position) {
        super(position);
        this.growthFactor = 0.0;
    }
    
    public Hurdle(Double position, Track parent) {
        super(position, 0.0, parent);
        this.growthFactor = 0.0;
    }
    
    public Hurdle(Double position, Double growthFactor, Double timeFactor, Track parent) {
        super(position, timeFactor, parent);
        this.growthFactor = growthFactor;
        this.originalPosition = position;
    }
    
    public Double getGrowthFactor() {
        return growthFactor;
    }
}
