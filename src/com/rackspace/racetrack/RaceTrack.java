/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rackspace.racetrack;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author mich4570
 */
public class RaceTrack {
    private ArrayList<Track> tracks;
    
    public RaceTrack() {
        tracks = new ArrayList<Track>();
    }
    
    public RaceTrack(String filename) throws FileNotFoundException, MalformedURLException, IOException {
        URL url = new URL(filename);
        Scanner scanner = new Scanner (new InputStreamReader((InputStream)url.getContent()));
        //Scanner scanner = new Scanner(new FileInputStream(filename));
        Track newTrack = null;
        Runner runner;
        Hurdle hurdle;
        
        tracks = new ArrayList<Track>();
        
        Pattern trackPattern = Pattern.compile("TRACK ([0-9]+\\.0)");
        Pattern runnerPattern = Pattern.compile("\t*RUNNER (0\\.+[0-9]*)");
        Pattern hurdlePattern = Pattern.compile("\t*HURDLE (0\\.+[0-9]*)");
        Pattern linkPattern = Pattern.compile("LINK TRACK([1-9]+)\\.(RUNNER|HURDLE)([1-9]*),[ \t]*TRACK([1-9]+)\\.(RUNNER|HURDLE)([1-9]*)");
        Matcher m;
        
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            
            if (line.startsWith("TRACK")) {
                double requiredTime = 0.0;
                m = trackPattern.matcher(line);
                if (m.matches()) {
                    System.out.println("TRACK FOUND " + m.group(1));
                    newTrack = new Track(Double.parseDouble(m.group(1)));
                    if (newTrack != null)
                        this.addTrack(newTrack);
                }
            }
            else if (line.startsWith("\tRUNNER")) {
                m = runnerPattern.matcher(line);
                if (m.matches()) {
                    System.out.println("\tRUNNER FOUND " + m.group(1));
                    runner = new Runner(Double.parseDouble(m.group(1)));
                    if (newTrack != null)
                        newTrack.setRunner(runner);
                }
            }
            else if (line.startsWith("\tHURDLE")) {
                m = hurdlePattern.matcher(line);
                if (m.matches()) {
                    System.out.println("\tHURDLE FOUND " + m.group(1));
                    hurdle = new Hurdle(Double.parseDouble(m.group(1)));
                    if (newTrack != null)
                        newTrack.addHurdle(hurdle);
                }
            }
            else if (line.startsWith("LINK")) {
                m = linkPattern.matcher(line);
                if (m.matches()) {
                    System.out.println("LINK FOUND " + m.group(1) + " " + m.group(2) + " " + m.group(3) + " " + m.group(4) + " " + m.group(5) + " " + m.group(6));
                    
                    Track t = this.getTrack(Integer.parseInt(m.group(1)) - 1);
                    TrackElement t1, t2;
                    
                    t1 = null;
                    t2 = null;
                    
                    if (m.group(2).equals("RUNNER")) {
                        t1 = t.getRunner();
                    }
                    else if (m.group(2).equals("HURDLE")) {
                        t1 = t.getHurdle(Integer.parseInt(m.group(3)) - 1);
                    }
                    
                    t = this.getTrack(Integer.parseInt(m.group(4)) - 1);
                    
                    if (m.group(5).equals("RUNNER")) {
                        t2 = t.getRunner();
                    }
                    else if (m.group(5).equals("HURDLE")) {
                        t2 = t.getHurdle(Integer.parseInt(m.group(3)) - 1);
                    }
                    
                    if (t1 != null && t2 != null)
                        TrackElement.link(t1, t2);
                }
            }
        }
    }
    
    public Track addTrack(Track track) {
        tracks.add(track);
        return track;
    }
    
    public Track getTrack(Integer index) {
        return tracks.get(index);
    }
    
    public ArrayList<Track> getTracks() {
        return tracks;
    }
    
    public Integer getNumberOfTracks() {
        return tracks.size();
    }
    
    public Track getLongestTrack() {
        Track longestTrack = tracks.get(0);
        for (Track track : tracks) {
            if (track.getBaseRequiredTime() > longestTrack.getBaseRequiredTime()) {
                longestTrack = track;
            }
        }
        
        return longestTrack;
    }
    
    public Double getOriginalScale(Integer trackNumber) {
        Track longestTrack = getLongestTrack();
        
        return tracks.get(trackNumber).getBaseRequiredTime()/longestTrack.getBaseRequiredTime();
    }
    
    public Double getOriginalScale(Track track) {
        Track longestTrack = getLongestTrack();
        
        return track.getBaseRequiredTime()/longestTrack.getBaseRequiredTime();
    }
    
    public Double getCurrentScale(Integer trackNumber) {
        Track longestTrack = getLongestTrack();
        
        return tracks.get(trackNumber).getTrackTime()/longestTrack.getTrackTime();
    }
    
    public Double getCurrentScale(Track track) {
        Track longestTrack = getLongestTrack();
        
        return track.getTrackTime()/longestTrack.getTrackTime();
    }
    
    public Long getOriginalTrackSizeInPixels(Integer trackNumber, Integer maxWidth) {
        return Math.round(getOriginalScale(trackNumber) * maxWidth);
    }
    
    public Long getOriginalTrackSizeInPixels(Track track, Integer maxWidth) {
        return Math.round(getOriginalScale(track) * maxWidth);
    }
    
    public Long getCurrentTrackSizeInPixels(Integer trackNumber, Integer maxWidth) {
        return Math.round(getCurrentScale(trackNumber) * maxWidth);
    }
    
    public Long getCurrentTrackSizeInPixels(Track track, Integer maxWidth) {
        return Math.round(getCurrentScale(track) * maxWidth);
    }
   
    public void displayInformation() {
        for (Track track: tracks) {
            System.out.println("Expected finish time: " + track.getRequiredTime() + " (" + track.getBaseRequiredTime() + ")");
            System.out.println("Pixel Width: " + getCurrentTrackSizeInPixels(track, 800) + "px");
            System.out.println("Runner Position: " + track.getRunner().getPosition() + " (" + track.getRunner().getOriginalPosition() + ")");
            for (Hurdle hurdle: track.getHurdles()) {
                System.out.println("\tHurdle Position: " + hurdle.getPosition() + " (" + hurdle.getOriginalPosition() + ")" + " [" + hurdle.getDeltaPosition() +  "]");
            }
            System.out.println("");
        }
    }
}
