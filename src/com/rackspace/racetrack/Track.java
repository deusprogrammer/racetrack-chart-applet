/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rackspace.racetrack;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class Track {
    private Runner runner;
    private ArrayList<Hurdle> hurdles;
    private Double baseRequiredTime, requiredTime;
    
    public Track(Double baseRequiredTime) {
        runner = null;
        hurdles = new ArrayList<Hurdle>();
        this.requiredTime = this.baseRequiredTime = baseRequiredTime;
    }
    
    public void setRunner(Runner runner) {
        runner.setParent(this);
        this.runner = runner;
    }
    
    public Hurdle getHurdle(Integer index) {
        return hurdles.get(index);
    }
    
    public Hurdle addHurdle(Double originalPosition, Double growthFactor, Double timeFactor) {
        hurdles.add(new Hurdle(originalPosition, growthFactor, timeFactor, this));
        return hurdles.get(hurdles.size());
    }
    
    public Hurdle addHurdle(Hurdle hurdle) {
        hurdle.setParent(this);
        hurdles.add(hurdle);
        return hurdle;
    }
    
    public Hurdle addHurdle(Double originalPosition) {
        hurdles.add(new Hurdle(originalPosition, this));
        return hurdles.get(hurdles.size());
    }
    
    public Double getTrackTime() {
        Double delta = 0.0;
        
        for (Hurdle hurdle : hurdles) {
            delta += hurdle.getDeltaPosition();
        }
        
        return requiredTime + (requiredTime * delta);
    }
    
    public Double getRequiredTime() {
        if (runner == null) {
            return 0.0;
        }
        
        Double delta = 0.0;
        
        for (Hurdle hurdle : hurdles) {
            delta += hurdle.getDeltaPosition();
        }
        
        return (requiredTime - (requiredTime * runner.position)) + (requiredTime * delta);
    }
    
    public Double getBaseRequiredTime() {
        return baseRequiredTime;        
    }
    
    public Runner getRunner() {
        return runner;
    }
    
    public ArrayList<Hurdle> getHurdles() {
        return hurdles;
    }
}
