/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rackspace.racetrack;

/**
 *
 * @author user
 */
public class Runner extends TrackElement {
    
    public Runner(Double timeFactor, Track parent)
    {
        super(0.0, timeFactor, parent);
    }
    
    public Runner(Track parent)
    {
        super(0.0, 0.0, parent);
    }
    
    public Runner(Double startingPosition) 
    {
        super(startingPosition);
    }
    
    public Runner()
    {
        super(0.0);
    }
}
