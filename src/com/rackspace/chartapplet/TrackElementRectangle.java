package com.rackspace.chartapplet;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import com.rackspace.racetrack.Track;
import com.rackspace.racetrack.TrackElement;
import java.awt.Rectangle;

/**
 *
 * @author user
 */
public class TrackElementRectangle extends Rectangle {
    private TrackElement element;
    private Track parent;
    
    public TrackElementRectangle(Rectangle rectangle, TrackElement element, Track parent)
    {
        this.x = rectangle.x;
        this.y = rectangle.y;
        this.width = rectangle.width;
        this.height = rectangle.height;
        this.element = element;
        this.parent = parent;
    }
    
    public TrackElementRectangle(int x, int y, int width, int height, TrackElement element, Track parent)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.element = element;
        this.parent = parent;
    }
    
    public TrackElement getElement() 
    {
        return element;
    }
    
    public Track getParent()
    {
        return parent;
    }
}