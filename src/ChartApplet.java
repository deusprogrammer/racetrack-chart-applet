

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.rackspace.chartapplet.TrackElementRectangle;
import com.rackspace.racetrack.*;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class ChartApplet extends Applet implements MouseListener, MouseMotionListener {
    int deltaX, deltaY;
    int x, y;
    int startX, startY;
    boolean rectClicked = false;
    boolean moving = false;
    
    RaceTrack rt;
    Track track1, track2, track3, track4;
    Runner runner1, runner2, runner3, runner4;
    Hurdle hurdle11, hurdle12, hurdle13;
    Hurdle hurdle21, hurdle22, hurdle23, hurdle24;
    Hurdle hurdle31;
    Hurdle hurdle41, hurdle42;
    
    Graphics bufferGraphics; 
    Image offscreen; 
    Dimension dim;
    
    double deltaPos;
    
    ArrayList<TrackElementRectangle> rectangles;
    TrackElementRectangle selected;

    /**
     * Initialization method that will be called after the applet is loaded into
     * the browser.
     */
    @Override
    public void init() {
        try {
            String configFile = getCodeBase().toString() + "racetrack.config";
            System.out.println("CONFIG: " + configFile);
            rt = new RaceTrack(configFile);
        } catch (Exception ex) {
            System.out.println("Couldn't find config file");
        }
        
        /*
        rt = new RaceTrack();
        
        track1 = new Track(30.0);
        track2 = new Track(100.0);
        track3 = new Track(25.0);
        track4 = new Track(7.0);
        
        runner1 = new Runner();
        runner2 = new Runner();
        runner3 = new Runner();
        runner4 = new Runner();
        
        hurdle11 = new Hurdle(0.25);
        hurdle12 = new Hurdle(0.50);
        hurdle13 = new Hurdle(0.75);
        
        hurdle21 = new Hurdle(0.10);
        hurdle22 = new Hurdle(0.33);
        hurdle23 = new Hurdle(0.40);
        hurdle24 = new Hurdle(0.60);
        
        hurdle31 = new Hurdle(0.22);
        
        hurdle41 = new Hurdle(0.50);
        hurdle42 = new Hurdle(0.90);
        
        track1.setRunner(runner1);
        track1.addHurdle(hurdle11);
        track1.addHurdle(hurdle12);
        track1.addHurdle(hurdle13);
        
        track2.setRunner(runner2);
        track2.addHurdle(hurdle21);
        track2.addHurdle(hurdle22);
        track2.addHurdle(hurdle23);
        track2.addHurdle(hurdle24);
                
        track3.setRunner(runner3);
        track3.addHurdle(hurdle31);
        
        track4.setRunner(runner4);
        track4.addHurdle(hurdle41);
        track4.addHurdle(hurdle42);
        
        TrackElement.link(runner1, runner2);
        TrackElement.link(hurdle22, hurdle23);
        TrackElement.link(hurdle11, hurdle22);
        TrackElement.link(hurdle12, hurdle31);
                
        rt.addTrack(track1);
        rt.addTrack(track2);
        rt.addTrack(track3);
        rt.addTrack(track4);
        */
        
        setBackground(Color.white);
        
        dim = getSize();
        offscreen = createImage(dim.width, dim.height);
        bufferGraphics = offscreen.getGraphics(); 
        
        addMouseListener(this);
        addMouseMotionListener(this);
    }
    // TODO overwrite start(), stop() and destroy() methods
    
    @Override
    public void paint(Graphics g) {
        int row = 0;
        bufferGraphics.clearRect(0,0,dim.width,dim.height); 
        //bufferGraphics.fillRect(rect.x, rect.y, rect.width, rect.height);
        
        int pixelsPerTrack = (dim.height / rt.getNumberOfTracks()) - 10;
        rectangles = new ArrayList<TrackElementRectangle>();
                
        for (Track track : rt.getTracks()) {
            int max = (int)dim.width - 40;
            int width = (int)rt.getCurrentTrackSizeInPixels(track, max).intValue();
            int rPos = (int)(20 + (track.getRunner().getPosition() * width));
            //int rWidth = (int)(10 * rt.getCurrentScale(track));
            int rWidth = 10;

            //Draw remaining time on project
            bufferGraphics.drawString(Math.round(track.getRequiredTime()) + " days left", 0, (pixelsPerTrack * row) + 20);
            
            //Draw track
            bufferGraphics.setColor(Color.green);
            bufferGraphics.fillRect(20, (pixelsPerTrack * row) + 20, width, pixelsPerTrack - 10);
            
            //Draw hurdles
            for (Hurdle hurdle : track.getHurdles()) {
                int hPos = (int)(20 + (hurdle.getPosition() * width));
                //int hWidth = (int)(10 * rt.getCurrentScale(track));
                int hWidth = 10;
                rectangles.add(new TrackElementRectangle(hPos, (pixelsPerTrack * row) + 25, hWidth, pixelsPerTrack - 20, hurdle, track));
                
                bufferGraphics.setColor(Color.white);
                bufferGraphics.fillRect(hPos, (pixelsPerTrack * row) + 25, hWidth, pixelsPerTrack - 20);
            }
            
            //Draw runner
            bufferGraphics.setColor(Color.black);
            bufferGraphics.fillRect(rPos, (pixelsPerTrack * row) + 25, rWidth, pixelsPerTrack - 20);
            rectangles.add(new TrackElementRectangle(rPos, (pixelsPerTrack * row) + 25, rWidth, pixelsPerTrack - 20, track.getRunner(), track));
            
            //Draw the total time the project takes
            
            row++;
        }
        
        //Draw tracks here
        
        g.drawImage(offscreen, 0, 0, this);
    }
    
    @Override
    public void update(Graphics g) 
    { 
        dim = getSize();
        offscreen = createImage(dim.width, dim.height);
        bufferGraphics = offscreen.getGraphics(); 
        paint(g); 
    } 

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mousePressed(MouseEvent e) {
        startX = e.getX();
        startY = e.getY();
        
        //System.out.println("Mouse pressed...");
        //System.out.println("X: " + x + "\nY:" + y);
        
        for (TrackElementRectangle rect : rectangles) {
            if (rect.contains(startX, startY)) {
                System.out.println("Rectangle clicked");
                
                selected = rect;
                
                deltaX = startX - selected.x;
                deltaY = startY - selected.y;
                break;
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (selected != null) {
            selected.getElement().savePosition();
            selected = null;
            repaint();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    @Override
    public void mouseDragged(MouseEvent e) {
        x = e.getX();
        y = e.getY();
        
        if (selected != null) {
            int deltaSPos = (x - deltaX) - selected.x;
            int trackSize = (rt.getOriginalTrackSizeInPixels(selected.getParent(), dim.width - 40)).intValue();
            double dPos = (double)deltaSPos/(double)trackSize;
            
            selected.getElement().moveRelative(dPos);
            
            repaint();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }
}
